.PHONY: all

info: intro usage

intro:
	@echo "\n    Vault - Demo application for browser testing with Laravel Dusk\n"

usage:
	@echo "Project:"
	@echo "  make dusk        		Rebuild the database and run the dusk browser tests"
	@echo "  make migrate			Rebuild the database from scratch and run migrations + seeds."
	@echo "  make codestyle			Check the codestyle"
	@echo "  make codestyle-fix		Fix the codestyle"

dusk: intro do_migrate do_dusk
migrate: intro do_migrate
codestyle: intro do_codestyle
codestyle-fix: intro do_codestyle_fix

do_migrate:
	php artisan migrate:fresh --seed --force

do_dusk:
	php artisan dusk

do_codestyle:
	vendor/bin/ecs check --config=dev/ecs/vault.yml --no-progress-bar .

do_codestyle_fix:
	vendor/bin/ecs check --config=dev/ecs/vault.yml --no-progress-bar --fix .


