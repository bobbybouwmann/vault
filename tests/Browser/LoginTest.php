<?php

declare(strict_types=1);

namespace Tests\Browser;

use App\User;
use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;
use Throwable;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        factory(User::class)->create([
            'email' => 'bobby@vault.com',
            'password' => Hash::make('password'),
        ]);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testLoginWithUser(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new LoginPage())
                ->type('email', 'bobby@vault.com')
                ->type('password', 'password')
                ->press('Login')
                ->pause(1000)
                ->on(new CredentialsPage());
        });
    }
}
