<?php

declare(strict_types=1);

namespace Tests\Browser;

use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;
use Throwable;

class UpdateCredentialTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->createCredential([
            'name' => 'UpdateCredential',
            'url' => 'https://credential.com/update',
            'username' => 'update',
            'password' => encrypt('update'),
        ]);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testUpdateCredential(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->click('.card-link')
                ->waitFor('.modal.show')
                ->assertInputValue('name', 'UpdateCredential')
                ->assertInputValue('url', 'https://credential.com/update')
                ->assertInputValue('username', 'update')
                ->assertInputValue('password', 'update')
                ->type('name', 'UpdateCredential2')
                ->type('url', 'https://credential.com/update/2')
                ->type('username', 'username-update')
                ->type('password', 'password-update')
                ->press('Save credential')
                ->waitUntilMissing('.modal.show')
                ->assertSee('Showing 1 credentials')
                ->assertSeeIn('.card-title', 'UpdateCredential2')
                ->assertSee('https://credential.com/update')
                ->assertSee('username-update')
                ->assertSee('**********')
                ->assertDontSee('password-update');
        });
    }
}
