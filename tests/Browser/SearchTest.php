<?php

declare(strict_types=1);

namespace Tests\Browser;

use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;
use Throwable;

class SearchTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->createDefaultCredentials();
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testSearchingForCredentialsByName(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->assertSee('Showing 6 credentials')
                ->type('search', 'amazon')
                ->assertSee('Showing 2 credentials');
        });
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testSearchingForCredentialsByUrl(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->assertSee('Showing 6 credentials')
                ->type('search', 'https://google.com')
                ->assertSee('Showing 1 credentials');
        });
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testSearchingForCredentialsByUsername(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->assertSee('Showing 6 credentials')
                ->type('search', 'superman-')
                ->assertSee('Showing 2 credentials');
        });
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testSearchingForCredentialsByTypingJiberish(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->assertSee('Showing 6 credentials')
                ->type('search', 'Jiberish')
                ->assertSee('Showing 0 credentials');
        });
    }
}
