<?php

declare(strict_types=1);

namespace Tests\Browser;

use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;
use Throwable;

class ShowPasswordTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->createCredential([
            'password' => encrypt('secret-password'),
        ]);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testShowPasswordOfCredential(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader');

            // Perform click on hidden password
            $browser->script("return document.querySelector('.text-monospace').click();");

            $browser->waitForText('secret-password');
        });
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testShowPasswordDisappearsAfterTime(): void
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader');

            $browser->script("return document.querySelector('.text-monospace').click();");

            $browser
                ->pause(1000)
                ->assertSee('secret-password')
                ->assertDontSee('**********')
                ->pause(10000)
                ->assertDontSee('secret-password')
                ->assertSee('**********');
        });
    }
}
