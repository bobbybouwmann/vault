<?php

declare(strict_types=1);

namespace Tests\Browser;

use Exception;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;
use Throwable;

class DeleteCredentialTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->createCredential([
            'name' => 'DeleteCredential',
            'url' => 'https://credential.com/delete',
            'username' => 'delete',
            'password' => encrypt('delete'),
        ]);
    }

    /**
     * @throws Exception
     * @throws Throwable
     */
    public function testDeleteCredential(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->click('.card-link:nth-child(2)')
                ->waitFor('.swal2-shown')
                ->press('Yes, Delete credential!')
                ->press('OK')
                ->waitUntilMissing('.swal2-shown')
                ->waitForText('Showing 0 credentials')
                ->assertDontSee('.card-title', 'DeleteCredential')
                ->assertDontSee('https://credential.com/delete')
                ->assertDontSee('delete')
                ->assertDontSee('**********');
        });
    }
}
