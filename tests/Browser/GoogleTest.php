<?php

declare(strict_types=1);

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Throwable;

class GoogleTest extends DuskTestCase
{
    /**
     * @throws Throwable
     */
    public function testGooglingPhpDay(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://google.com')
                ->type('q', 'PHPDay')
                ->keys('input[name="q"]', '{enter}')
                ->assertInputValue('q', 'PHPDay')
                ->assertSee('phpDay 2019 __ Welcome')
                ->assertSeeLink('https://www.phpday.it/');
        });
    }
}
