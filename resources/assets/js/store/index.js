import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isLoggedIn: !!localStorage.getItem('token'),
    search: localStorage.hasOwnProperty('search') ? localStorage.getItem('search') : ''
  },

  mutations: {
    loginUser (state) {
      state.isLoggedIn = true;
    },
    logoutUser (state) {
      state.isLoggedIn = false;
    },
    search (state, search) {
      state.search = search;
    }
  }
});
