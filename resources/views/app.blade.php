<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Vault</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
</head>
<body>

<div id="app"></div>

@javascript('url', config('app.url'))

<script type="text/javascript">
  let config = {
    host: url
  };
</script>
<script type="text/javascript" src="{{ asset('js/app.js')  }}"></script>

</body>
</html>
